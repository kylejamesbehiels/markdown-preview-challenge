import logo from './logo.svg';
import './App.scss';
import Home from './Pages/Home';
import { Helmet } from 'react-helmet';

function App() {
  return (
    <div className="App">
      <Helmet>
        <title>Markdown Previewer</title>
      </Helmet>
      <Home></Home>
    </div>
  );
}

export default App;

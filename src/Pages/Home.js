import React, { Component } from 'react'
import { FaClipboard } from 'react-icons/fa';


const marked = require("marked");
const DOMPurify = require("dompurify");

export default class Home extends Component {

    constructor(){
        super();
        this.state={
            html: "",
            markdown:""
        }

        this.getMarkdownAndConvert = this.getMarkdownAndConvert.bind(this);
        this.saveToClipboard = this.saveToClipboard.bind(this);

    }
    
    componentDidMount(){
        var localStorageMarkdown = localStorage.getItem("markdown");
        if(localStorageMarkdown){
            this.setState({
                markdown: localStorageMarkdown,
                html: DOMPurify.sanitize(marked(localStorageMarkdown))
            })
        }
    }

    getMarkdownAndConvert(e){
        // Sanitize HTML to prevent XSS
        this.setState({
            markdown: e.target.value,
            html: DOMPurify.sanitize(marked(e.target.value))
        }, () => {
            // Save markdown locally
            localStorage.setItem("markdown", this.state.markdown)
        });
    }

    saveToClipboard(){
        const ta = document.getElementById("markdown_textarea");
        ta.select();
        document.execCommand('copy');
        alert("Successfully copied markdown to clipboard");

    }

    render() {
        return (
            <div>
                <div className="jumbotron">
                    <div className="container">
                        <h2>Welcome to my Markdown Previewer! <hr></hr> <small>New to markdown? Check out the guide <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">here!</a></small></h2>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <h3>Markdown</h3>
                            <hr></hr>
                            <textarea id="markdown_textarea" className="form-control" value={this.state.markdown} rows={this.state.markdown.split(/\r\n|\r|\n/).length < 15 ? "15" : this.state.markdown.split(/\r\n|\r|\n/).length} placeholder="Markdown" onChange={this.getMarkdownAndConvert}></textarea>
                            <div className="padded text-center">
                                <button className="btn btn-primary" onClick={this.saveToClipboard}>Copy Markdown to Clipboard <FaClipboard></FaClipboard></button>
                            </div>
                        </div>
                        <div className="col-6">
                            <h3>HTML Preview</h3>
                            <hr></hr>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: this.state.html
                                }}
                            ></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

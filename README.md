# Simple web application for demonstrating a markdown previewer in React

This application is a response to the challenge posed [here](https://github.com/florinpop17/app-ideas/blob/master/Projects/2-Intermediate/Markdown-Previewer.md), is written in javascript and SASS and leverages both React and Bootstrap.

## Running the application locally

NOTE: You will need the following packages `git npm nodejs`

### For Development

1. Clone this repository `git clone https://gitlab.com/kylejamesbehiels/markdown-preview-challenge`
3. Install dependencies `npm install`
4. Run the application locally `npm start`

### For Production

1. Clone this repository `git clone https://gitlab.com/kylejamesbehiels/markdown-preview-challenge`
3. Install dependencies `npm install`
4. Build the application `npm run build`
5. (Optional) Run the built application `npm serve`
